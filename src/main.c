/*
** main.c for main in /home/mar_b/rendu/sudoki-bi
**
** Made by mar_b
** Login   <mar_b@epitech.net>
**
** Started on  Fri Jul 25 21:55:42 2014 mar_b
** Last update Sat Jul 26 16:46:23 2014 mar_b
*/

int		main(int __attribute__((__unused__)) argc,
		     char __attribute__((__unused__)) **argv)
{
  char		start;

  start = 0;		//a 0 quand on a pas encore parcouru notre fichier sudoku puis on le set a 1 apres
  while (parser(start)) //Appelle tant qu'on n'est pas a la fin du fichier
    start = 1;
  return (0);
}
