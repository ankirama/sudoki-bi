/*
** solver.c for solver in /home/mar_b/rendu/sudoki-bi
**
** Made by mar_b
** Login   <mar_b@epitech.net>
**
** Started on  Fri Jul 25 22:03:59 2014 mar_b
** Last update Sat Jul 26 18:38:44 2014 mar_b
*/

#include "solver.h"

/*
** brief: it will check if we can play this value
** @sudo: our current sudoku
** @value: our current value (1-9)
** @row: our current row
** @column: our current column
** BRIEF: we will check the entire row/column and the square
** return: return 0 if isn't valid else 1
*/
static char	_isValid(char sudo[9][9], char value,
			 char row, char column)
{
  char		i;
  char		column1;
  char		column2;
  char		row1;
  char		row2;

  /*
  ** les 4 variables permettent de checker le carre ou se trouve notre nombre
  */
  row1 = (row + 2) % 3 + (3 * (row / 3));
  row2 = (row + 4) % 3 + (3 * (row / 3));
  column1 = (column + 2) % 3 + (3 * (column / 3));
  column2 = (column + 4) % 3 + (3 * (column / 3));
  i = -1;
  while (++i < 9)
    {
      if (sudo[row][i] == value || sudo[i][column] == value) //on test si jamais le nombre est deja present sur la colone ou rangee
	return (0);
    }
  /*
  ** ici on check notre carre
  */
  if (sudo[row1][column1] == value || sudo[row1][column2] == value)
    return (0);
  if (sudo[row2][column1] == value || sudo[row2][column2] == value)
    return (0);
  return (1);
}

/*
** brief: this is our main function for solve our sudoku
** @sudo: our current sudoku
** @row, @column: our position
** return: return 0 if the sudoku isn't good, else 1
*/
char   	solveSudoku(char sudo[9][9], char row, char column)
{
  char	value;

  if (row == 9) //si jamais row = 9 alors on a finit de remplir notre tableau, on retourne 1 on a reussi !
    return (1);
  /*
  ** s'il y a un nombre sur notre tableau deja present, alors on va essayer
  ** de rappeler recursivement notre fonction en rajoutant 1 a la colone, sauf si on est deja a la fin de la colone,
  ** dans ce cas on passe a la range suivante
  ** on retourne un si jamais recursivement tout est ok, sinon 0 -> erreur
  */
  if (sudo[row][column] != 0)
    {
      if ((column == 8 && solveSudoku(sudo, row + 1, 0)) ||
	  solveSudoku(sudo, row, column + 1))
	return (1);
      return (0);
    }
  value = 0;
  /*
  ** on va essayer en partant de 1 jusqu'a 9 (inclu) de remplir notre sudoku, donc on essaye avec 1, on verifie si c'est correct avec isValid, sinon on essaye avec 2
  ** sans oublier de reinitialiser la valeur actuelle...
  */
  while (++value < 10)
    {
      if (_isValid(sudo, value, row, column))
	{
	  sudo[row][column] = value;
	  if ((column == 8 && solveSudoku(sudo, row + 1, 0)) ||
	      solveSudoku(sudo, row, column + 1))
	    return (1);
	}
      sudo[row][column] = 0;
    }
  return (0);
}
