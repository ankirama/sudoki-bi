/*
** utils.h for utils in /home/mar_b/rendu/sudoki-bi
**
** Made by mar_b
** Login   <mar_b@epitech.net>
**
** Started on  Sat Jul 26 10:35:51 2014 mar_b
** Last update Sat Jul 26 14:48:23 2014 mar_b
*/

#ifndef UTILS_H_
# define UTILS_H_

void	my_puterror(char *str);
void	displaySudoku(char array[9][9], char start);

#endif /* !UTILS_H_ */
