/*
** utils.c for utils in /home/mar_b/rendu/sudoki-bi
**
** Made by mar_b
** Login   <mar_b@epitech.net>
**
** Started on  Sat Jul 26 10:34:36 2014 mar_b
** Last update Sat Jul 26 14:46:13 2014 mar_b
*/

#include <unistd.h>
#include <stdlib.h>
#include <string.h>

void	my_puterror(char *str)
{
  (void)write(2, str, strlen(str));
  exit(EXIT_FAILURE);
}

/*
** brief: it will display on stdout our Sudoku
** @array: our sudoku
** @start: start = 0 : it's our first sudoku else no
*/
void	displaySudoku(char array[9][9], char start)
{
  char	column;
  char	row;
  char	letter;

  if (start != 0)
    (void)write(1, "####################\n", 21);
  row = -1;
  (void)write(1, "|------------------|\n", 21);
  while (++row < 9)
    {
      column = -1;
      (void)write(1, "| ", 2);
      while (++column < 9)
	{
	  letter = array[row][column] + '0';
	  (void)write(1, &letter, 1);
	  if (column < 8)
	    (void)write(1, " ", 1);
	}
      (void)write(1, "|\n", 2);
    }
  (void)write(1, "|------------------|\n", 21);
}
