/*
** parser.h for parser in /home/mar_b/rendu/sudoki-bi/src
**
** Made by mar_b
** Login   <mar_b@epitech.net>
**
** Started on  Sat Jul 26 02:07:50 2014 mar_b
** Last update Sat Jul 26 14:48:34 2014 mar_b
*/

#ifndef PARSER_H_
# define PARSER_H_

char	parser(char start);

#endif /* !PARSER_H_ */
