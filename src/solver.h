/*
** solver.h for solver in /home/mar_b/rendu/sudoki-bi
**
** Made by mar_b
** Login   <mar_b@epitech.net>
**
** Started on  Fri Jul 25 22:25:39 2014 mar_b
** Last update Sat Jul 26 01:54:41 2014 mar_b
*/

#ifndef SOLVER_H_
# define SOLVER_H_

char	solveSudoku(char sudo[9][9], char row, char column);

#endif /* !SOLVER_H_ */
