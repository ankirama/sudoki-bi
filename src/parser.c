/*
** parser.c for parser in /home/mar_b/rendu/sudoki-bi/src
**
** Made by mar_b
** Login   <mar_b@epitech.net>
**
** Started on  Sat Jul 26 01:58:23 2014 mar_b
** Last update Sat Jul 26 18:33:34 2014 mar_b
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "solver.h"
#include "utils.h"
#include "get_next_line.h"

/*
** brief: it will initialize our array
** @array: our sudoky array
*/
static void	_init_array(char array[9][9])
{
  int		row;
  int		column;

  /*
  ** on parcourt notre tableau et on met des 0 partout
  */
  row = -1;
  while (++row < 9)
    {
      column = -1;
      while (++column < 9)
	array[row][column] = 0;
    }
}

/*
** brief: will "convert" the current char *line in our array
** @s: our line like | 1 2 3 4 5 6 7 8 9|
** @array: our sudoku grid
** @row: our current row
** return: return 0 if it's the header (|-------...---|)
*/
static char	_add_line(char *s, char array[9][9], char row)
{
  int		column;
  int		i;

  if (row == -1 || row == 9)
    return (0);
  column = -1;
  i = 0;
  while (s[++i] != '\0' && i < 19) //boucle allant de i = 1 a i = 19, ce qui correspond a une ligne du type | 1 2 3 4 5 6 7 8 9|
    {
      if (i % 2 != 0 && s[i] != ' ') //Si jamais il n'y a pas d'espace aux endroits impair c'est qu'il y a un probleme
	my_puterror("error: the file isn't good: the seprator isn't a ' '\n");
      else if (i % 2 == 0)
	{
	  if (s[i] >= '1' && s[i] <= '9') //si s[i] >= '1' et s[i] <= '9' c'est que c'est un chiffre deja existant, on le met donc dans notre tableau array
	    array[row][++column] = s[i] - '0';
	  else if (s[i] == ' ') //si c'est pas un chiffre mais que c'est un espace, alors on met 0, ca veut dire que la valeur n'est pas definie
	    array[row][++column] = 0;
	  else //si ce n'est ni un espace ni un chiffre alors c'est une erreur !
	    my_puterror("error: the grid has to have only 1 -> 9 or space!\n");
	}
    }
  return (1);
}

/*
** brief: our main fun for our parser
** @start: start = 0 : it's our first sudoku else no
** return : return 0 if it's the end, else 1
*/
char	parser(char start)
{
  char	*s;
  char	array[9][9];
  char	row;

  _init_array(array); //initialisation buffer
  row = -1;
  while (++row < 11 && (s = get_next_line(0)) != NULL) //on va parcourir 11 lignes par 11 lignes, ce qui correspond a un sudoku (2 lignes pour le haut et bas et 9 pour le plateau)
    {
      if (strlen(s) != 20) //si jamais la ligne n'est pas == 20 alors elle n'est pas correcte
	my_puterror("error: problem the file isn't correct 3!\n");
      (void)_add_line(s, array, row - 1); //on appelle notre fonction pour remplir notre rangee dans le tableau
      free(s); //ne pas oublier de free
    }
  if (row > 0 && row < 11) //si jamais row > 0 mais < 11 ca veut dire que l'on a reussi a lire quelques lignes, ce n'est pas correct !
    my_puterror("error: problem the sudoku isn't correct 4!\n");
  if (row == 0) //si row = 0 ca veut dire qu'on est arrive a la fin du fichier, donc on quitte
    return (0);
  if (solveSudoku(array, 0, 0)) //c'est ici qu'on appelle notre solveur, il retourne 1 s'il a reussi, 0 sinon
    displaySudoku(array, start); //on affiche donc notre sudoku resolu
  else
    fprintf(stderr, "error: the sudoku isn't solvable!\n"); //s'il ne peut etre resolu, on affiche un message d'erreur sur la sortie d'erreurs puis on passe au sudoku suivant s'il existe
  return (1);
}
