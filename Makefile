##
## Makefile for Makefile in /home/mar_b/rendu/sudoki-bi
##
## Made by mar_b
## Login   <mar_b@epitech.net>
##
## Started on  Fri Jul 25 21:44:04 2014 mar_b
## Last update Sat Jul 26 10:39:24 2014 mar_b
##

CC	= gcc

RM	= rm -f

ROOT	= ./src

CFLAGS	= -I./src

NAME	= ./bin/sudoki-bi

SRCS	= ./src/main.c\
	  ./src/get_next_line.c\
	  ./src/utils.c\
	  ./src/solver.c\
	  ./src/parser.c

OBJS	= $(SRCS:.c=.o)

all:	$(NAME)

$(NAME): $(OBJS)
	@$(CC) -o $(NAME) $(OBJS) $(LDFLAGS)

%.o:	%.c
	@tput setaf 3
	@tput bold
	@printf "Compiling "
	@tput setaf 4
	@printf "%s\n" $(patsubst $(ROOT)/%,%,$^)
	@$(CC) $(CFLAGS) -c -o $@ $^
	@tput sgr0
	@tput cuf 68
	@tput cuu1
	@printf "[ "
	@tput setf 2
	@tput bold
	@printf "ok"
	@tput sgr0
	@printf " ]\n"

clean:
	@$(RM) $(OBJS)
	@tput setaf 3
	@tput bold
	@printf "Removing \t "
	@tput setaf 4
	@printf " File [.o]...\n"
	@tput sgr0
	@tput cuf 68
	@tput cuu1
	@printf "[ "
	@tput setaf 2
	@tput bold
	@printf "ok"
	@tput sgr0
	@printf " ]\n"

fclean: clean
	@$(RM) $(NAME)
	@tput setaf 3
	@tput bold
	@printf "Removing \t "
	@tput setaf 4
	@printf " File %s...\n", $(NAME)
	@tput sgr0
	@tput cuf 67
	@tput cuu1
	@printf "[ "
	@tput setf 2
	@tput bold
	@printf "ok"
	@tput sgr0
	@printf " ]\n"

re: fclean all

.PHONY: all clean fclean re
